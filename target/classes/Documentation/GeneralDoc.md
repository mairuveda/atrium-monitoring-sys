## Error handling


A list of error codes and a descriptions will be returned for each web service failure. The individual API error codes are detailed at the API level documentation, however common system error codes are listed below.

Code          | Description         | HTTP code
------------- | ------------------- | ---------
CREATEACCTSF  | Unable to Create Account from Salesforce| 500


Response Values

Code           | Description         | Description
-------------  | ------------------- | ---------
errorCode      | String              | The error code.
errorMessage   | String              | Error descriptive message.
additionalInfo | List<String>        | String list with additional information about the error.
